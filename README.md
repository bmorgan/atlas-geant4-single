# Testing of Single Geant4 Library for ATLAS Simulation Optimization

See [ATLASSIM-3150](https://its.cern.ch/jira/browse/ATLASSIM-3150) for full
requirements, discussion, and technical details.

This repo is purely to hold scripts to drive builds and profiling runs. The
actual code (Geant4/FullSimLight) is held in dedicated repos:

- https://gitlab.cern.ch/atlas-simulation-team/geant4
- https://gitlab.cern.ch/bmorgan/GeoModel
  - A fork of the [upstream repo](https://gitlab.cern.ch/GeoModelDev/GeoModel)
    with a branch for patches needed to build with/use the single library.
  - Follows the upstream master branch as closely as possible.

# Requirements
- CentOS7
  - Git
  - Bash
  - CVMFS, sft.cern.ch repository
  - _Optional_ geant4.cern.ch repository for Geant4 data
- macOS Catalina
  - Does work, but scripts/setup not yet implemented here
  - Assumes Homebrew!

# Code
The ATLAS Geant4 repo is checked out as two git submodules at:

- `submodules/geant4/10.6.2`: tracks the upstream [geant4-10.6.2-singlelibrarytest](https://gitlab.cern.ch/atlas-simulation-team/geant4/-/tree/geant4-10.6.2-singlelibrarytest) branch
- `submodules/geant4/10.5.1`: tracks the upstream [geant4-10.5.1-singlelibrarytest](https://gitlab.cern.ch/atlas-simulation-team/geant4/-/tree/geant4-10.5.1-singlelibrarytest) branch

These _should not be touched_ other than to update them if and when new commits
are made to the tracked branches.

The GeoModel fork for this study is checked out as a git submodule at:

- `submodules/GeoModel`: tracks the [atlas-singlelib-develop](https://gitlab.cern.ch/bmorgan/GeoModel/-/tree/atlas-singlelib-develop) branch

# Building Geant4/GeoModel for Testing
## Setting Up
To check out everything needed, you will need to clone using:

```
$ git clone --recursive <projecturl>
```

to ensure the submodules for Geant4 (and later GeoModel) are checked out. To
keep the main repo and submodules updated after initial cloning, run

```
$ git pull --recurse-submodules
```

On CentOS7, the current _reference_ build uses the LCG `LCG97a_python3` software
stack on the `x86_64-centos7-gcc8-opt` platform (TODO: add other GCCs?). The set
of packages used are:

- CMake 3.14.3
- clhep 2.4.1.3
- zlib 1.2.11
- expat 2.2.6
- XercesC 3.1.3
- eigen 3.3.7
- sqlite 3280000
- Pythia8 244

Before starting a build, first `source` the `SetupForDevelopment.sh` script in the
top level of this repo to check and setup these packages:

```console
$ . SetupForDevelopment.sh
```

NB: at present this doesn't perform any error checking!!

## Build Matrix and Bundles
This study involves a relatively large build matrix, with dimensions:

- Geant4
  - Version
  - Library style (multi or single)
  - Static libraries built with PIC
- fullSimLight
  - Link type (static or shared)

To provide a consistent set of binaries easily deployable and usable on clusters, a script is
provided to build and package up this matrix. This may be run as follows

```console
$ <pathtothisrepo>/scripts/bundle/BuildBundle.sh --prefix=INSTALLPREFIX --geant4-version=VERSION
```

This will set up a build environment, then build the matrix above given the requested geant4
version. The resultant builds are installed to a directory name `fullsimlight.bundle` under
the `INSTALLPREFIX` directory. This directory is also bundled into a `fullsimlight.bundle.tar.gz` file
in `INSTALLPREFIX`. All builds are paralleized using all available resource, so the
script may be run easily on batch systems.

The bundle is fully relocatable, so be moved wherever needed. The primary interface to the bundle
is the `runFullSimLight` program in the top level directory of the bundle. This may be run to
select the appropriate cell of the build matrix and run the corresponding `fullSimLight` program
with given arguments (see `runFullSimLight --help` for details)

In general, the bundle build should take care of most use cases, but if you need to build Geant4
and/or FullSimLight from scratch, please see the following sections.


### Building from Scratch: Geant4
To test/profile Geant4 for this study, there are several options contributing
to the overall build matrix:

- Geant4 version: 10.5.1 _or_ 10.6.2
- Library type: multi _or_ single
  - _multi_ is equivalent to the default Geant4 organisation of libraries
- Static libs: non-PIC _or_ PIC
- For dynamic library comparisons: bsymbolic (only single mode)
- _maybe others_

To build a specific element of the matrix, `cmake` should be invoked as
(assuming that the working directory is the root of this repository):

```
$ cmake -C scripts/build/geant4-<type>[-<picmode>].cmake -B <builddir> -S submodules/geant4/<version>
$ cmake --build <builddir> -jN
```

For example, to build Geant4 10.6.2 in single library mode with non-PIC static libraries
on a machine with 12 cores:

```
$ cmake -C scripts/build/geant4-single.cmake -B mybuild -S submodules/geant4/10.6.2
$ cmake --build mybuild -j12
```

or to build the same but with PIC static libraries:

```
$ cmake -C scripts/build/geant4-single-pic.cmake -B mybuild-pic -S submodules/geant4/10.6.2
$ cmake --build mybuild-pic -j12
```

_All_ of these builds use a a standard set of options, defined in the file `scripts/builds/ATLASG4CommonSettings.cmake`.
These are matched to current ATLAS settings for Geant4, and will use the `Release` build mode, i.e. optimized. Note that this file also points Geant4 to the
physics data libraries on Geant4's CVMFS repo (`/cvmfs/geant4.cern.ch`). For
full performance studies without potential inteference from a
cold cache/network issues, these can also be installed manually locally.

There is no requirement to install these builds, but if you wish to do so, then you
will additionally need to supply the `CMAKE_INSTALL_PREFIX` argument to `cmake` at configure
time.


### Building from Scratch: FullSimLight
To use FullSimLight to profile Geant4 for this study, a similar "matrix" to the above options for
Geant4, but with two entries:

- FullSimLight with shared Geant4 libraries
- FullSimLight with static Geant4 libraries
- TODO: _FullSimLight in "Athena" mode, using a plugin library that links to Geant4_

Of course, the overall set of builds is larger as the above must be multiplied by the Geant4
build matrix (version, multi/single library, pic/non-pic).

Once a build/install of Geant4 exists, FullSimLight may be configured/built against it as:
(assuming that the working directory is the root of this repository):

```
$ cmake -DCMAKE_PREFIX_PATH=<pathtog4build> -C scripts/build/fullsimlight-<mode>.cmake -B <builddir> -S submodules/GeoModel
```

Here, `<mode>` is `static` or `shared` is the type of Geant4 libraries you want to link against.
To build, for example, `FullSimLight` linked against static Geant4 libraries:

```
$ cmake -DCMAKE_PREFIX_PATH=<pathtog4build> -C scripts/build/fullsimlight-static.cmake -B <builddir> -S submodules/GeoModel
$ cmake --build <builddir> -jN
```

The builds of FullSimLight need not be installed, and can be run directly from their locations in the
build directory.


# Running FullSimLight for Performance Profiling
TODO:
- Geometry, use best knowledge tag:
  - https://gitlab.cern.ch/GeoModelATLAS/geometry-data/raw/master/geometry/geometry-ATLAS-R2-2016-01-02-01_with_EMEC_Wheel.db
  - Because startup can be long for full ATLAS geometry/materials/physics combinarion, trivial GDML for checking scripts/Pythia8 in `scripts/fullsimlight/dummy.gdml`.
- Magnetic Field: Use ATLAS field (not publically available)
- Event Generator:
  - Isotropic electrons/positrons/gammas ~10K sample
  - Pythia 13TeV ttbar, ~1K sample

