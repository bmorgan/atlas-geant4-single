#!/bin/bash
set -e

# Script to run fullsimlight from within bundle
# Sets up environment for external LCG requirement, then for
# specified fullsimlight+geant4 combo (inc. local geant4 data)
# NB: -pic modes only make sense for static Geant4
# --fullsimlight: shared, static
# --geant4: multi, single, multi-pic, single-pic

die() {
  printf '[runFullSimLight:ERROR]: %s\n' "$1" >&2
  exit 1
}

info() {
  printf '[runFullSimLight:INFO]: %s\n' "$1"
}

usage() {
  cat << EOF
usage: runFullSimLight --fullsimlight=FSLMODE --geant4=G4MODE -- FSLARGS
EOF
}

show_help() {
  usage
  cat << EOF
Wrapper for fullSimLight to select linking mode and Geant4 library type

  -h, --help                Print this information

  --dump-metadata           Print metadata about how this bundle was built

  --fullsimlight=FSLMODE    Choose fullSimLight linked with FSLMODE libraries
                            FSLMODE is mandatory and must be one of:

                              static          Direct linking to static Geant4
                              shared          Direct linking to shared Geant4
                              static-indirect Link to static Geant4 via shared
                                              FullSimLight library
                              shared-indirect Link to shared Geant4 via shared
                                              FullSimLight library

                            The -indirect modes must be used with a -pic geant4
                            mode.

  --geant4=G4MODE           Use fullSimLight linked against G4MODE libraries
                            G4MODE is mandatory and must be one of:

                              multi         Standard multiple libraries
                              single        Single "big" library
                              multi-pic     Standard multiple libraries, with
                                            static libraries built with PIC
                              single-pic    Single "big" library, with static
                                            library built with PIC

Arguments to fullSimLight itself can be passed after '--'
EOF
}

# - Initialize
# Our location
fslr_this_dir=$(cd `dirname $0` && pwd)

# Allowed modes
allowed_fsl_modes='^(shared|static|shared\-indirect|static\-indirect)$'
allowed_geant4_modes="^(multi|single|multi-pic|single-pic)$"

# Empty mode args so user MUST choose them
fslr_fsl_mode=
fslr_geant4_mode=

# - Parse args
while true; do
  case $1 in
    -h|--help)
      show_help
      exit
      ;;
    --dump-metadata)
      cat "${fslr_this_dir}/.buildmetadata" || die "could not locate bundle metadata file"
      exit
      ;;
    --fullsimlight)
      if [ "$2" ] ; then
        fslr_fsl_mode=$2
        shift
        if [[ ! "$fslr_fsl_mode" =~ $allowed_fsl_modes ]] ; then
          die "Incorrect FullSimLight mode '$fslr_fsl_mode'"
        fi
      else
        die "--fullsimlight option requires a non-empty argument"
      fi
      ;;
    --fullsimlight=?*)
      fslr_fsl_mode=${1#*=}
      if [[ ! "$fslr_fsl_mode" =~ $allowed_fsl_modes ]] ; then
        die "Incorrect FullSimLight mode '$fslr_fsl_mode'"
      fi
      ;;
    --fullsimlight=)
      die "--fullsimlight option requires a non-empty argument"
      ;;
    --geant4)
      if [ "$2" ] ; then
        fslr_geant4_mode=$2
        shift
        if [[ ! "$fslr_geant4_mode" =~ $allowed_geant4_modes ]] ; then
          die "Incorrect Geant4 mode '$fslr_geant4_mode'"
        fi
      else
        die "--geant4 option requires a non-empty argument"
      fi
      ;;
    --geant4=?*)
      fslr_geant4_mode=${1#*=}
      if [[ ! "$fslr_geant4_mode" =~ $allowed_geant4_modes ]] ; then
        die "Incorrect Geant4 mode '$fslr_geant4_mode'"
      fi
      ;;
    --geant4=)
      die "--geant4 option requires a non-empty argument"
      ;;
    --)
      shift
      break
      ;;
    -?*)
      echo "Unknown option ignored"
      ;;
    *)
      break
  esac
  shift
done

# Validate args and setup
if [ -z "${fslr_fsl_mode}" ] ; then
  die "--fullsimlight argument was not supplied"
fi

if [ -z "${fslr_geant4_mode}" ] ; then
  die "--geant4 argument was not supplied"
fi

# -library fsl arg can only used with -pic geant4 arg
if [[ "${fslr_fsl_mode}" =~ ^.*indirect$ && ! "${fslr_geant4_mode}" =~ ^.*pic ]] ; then
  die "${fslr_fsl_mode} is not compatible with ${fslr_geant4_mode}"
fi

# - Need bundle config before we can setup tags
# LCG setup is done by SetupForDevelopment.sh
source "${fslr_this_dir}/.SetupForDevelopment.sh"

# Geant4 data setup done by Geant4Data.sh
source "${fslr_this_dir}/.Geant4Data.sh"

requested_geant4_tag="geant4/${FSL_BUNDLE_GEANT4_VERSION}/${fslr_geant4_mode}"

# FSL tag is both how we linked to geant4 and whether we run the direct
# application or the version that links to a library
fslr_fsl_link=${fslr_fsl_mode%-indirect}
requested_fsl_tag="fullsimlight/${fslr_fsl_link}/${requested_geant4_tag}"

# - FullSimLight
export PATH="${fslr_this_dir}/${requested_fsl_tag}/bin:${PATH}"
export LD_LIBRARY_PATH="${fslr_this_dir}/${requested_fsl_tag}/lib64:${fslr_this_dir}/${requested_fsl_tag}/bin:${LD_LIBRARY_PATH}"

# - Geant4
export LD_LIBRARY_PATH="${fslr_this_dir}/${requested_geant4_tag}/lib64:${LD_LIBRARY_PATH}"

# - Run required fullsimlight with supplied args
if [[ "${fslr_fsl_mode}" =~ ^.*indirect$ ]] ; then
  info "starting fullSimLightViaLibrary(${fslr_fsl_mode},${fslr_geant4_mode}) $@"
  fullSimLightViaLibrary $@
  info "completed fullSimLightViaLibrary(${fslr_fsl_mode},${fslr_geant4_mode}) $@"
else
  info "starting fullSimLight(${fslr_fsl_mode},${fslr_geant4_mode}) $@"
  fullSimLight $@
  info "completed fullSimLight(${fslr_fsl_mode},${fslr_geant4_mode}) $@"
fi
