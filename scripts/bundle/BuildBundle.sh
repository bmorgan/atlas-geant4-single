#!/bin/bash

# All-in-one script to build a bundle of a given Geant4 version and FSL
# with all build modes handled. It's intended for use through a job
# submission system, but can be run locally if you have sufficient
# resource
# It can be run from anywhere, but must retain its position in this project

set -e

# - Helpers
die() {
  printf '[BuildBundle:ERROR]: %s\n' "$1" >&2
  exit 1
}

show_help() {
  echo "help"
}

# - Location of this script and project source dir
bb_this_dir=$(cd `dirname $0` && pwd)
bb_source_dir=$(cd $bb_this_dir/../../ && pwd)

# - Input parameters, allowed values
bb_geant4_version=10.6.2
bb_geant4_allowed_versions="10.5.1|10.6.2"
bb_install_prefix=$PWD

# - Parse command line arguments
while true; do
  case $1 in
    -h|--help)
      show_help
      exit
      ;;
    --prefix)
      if [ "$2" ] ; then
        bb_install_prefix=$2
        shift
      else
        die "--prefix option requires a non-empty argument"
      fi
      ;;
    --prefix=?*)
      bb_install_prefix=${1#*=}
      ;;
    --prefix=)
      die "--prefix option requires a non-empty argument"
      ;;
    --geant4-version)
      if [ "$2" ] ; then
        bb_geant4_version=$2
        shift
        if [[ ! "$bb_geant4_version" =~ $bb_geant4_allowed_versions ]] ; then
          die "Unsupported Geant4 version '$bb_geant4_version'"
        fi
      else
        die "--geant4-version option requires a non-empty argument"
      fi
      ;;
    --geant4-version=?*)
      bb_geant4_version=${1#*=}
      if [[ ! "$bb_geant4_veraion" =~ $bb_geant4_allowed_versions ]] ; then
        die "Unsupported Geant4 version '$bb_geant4_version'"
      fi
      ;;
    --geant4-version=)
      die "--geant4-mode option requires a non-empty argument"
      ;;

    -?*)
      echo "Unknown option ignored"
      ;;
    *)
      break
  esac
  shift
done


bb_bundle_name="fullsimlight.bundle"
bb_bundle_dir="${bb_install_prefix}/${bb_bundle_name}"
if [ -d "${bb_bundle_dir}" ] ; then
  die "${bb_bundle_name} already exists in ${bb_install_prefix}"
fi

echo "[BuildBundle:INFO] Setting up..."

mkdir -p ${bb_bundle_dir} || die "failed to create ${bb_bundle_name} at ${bb_install_prefix}"
bb_build_dir=$(mktemp -d -t fsl-bundle-build-XXXXXXXX)

# - Setup the build environment
source ${bb_source_dir}/SetupForDevelopment.sh

# - Build combinations
echo "[BuildBundle:INFO] Starting build phase..."

bb_geant4_variants="multi single multi-pic single-pic"
bb_fsl_variants="shared static"

for v in ${bb_geant4_variants} ; do
  bb_build_geant4_tag=geant4/${bb_geant4_version}/${v}
  cmake \
    -C ${bb_source_dir}/scripts/build/geant4-${v}.cmake \
    -B ${bb_build_dir}/${bb_build_geant4_tag} \
    -S ${bb_source_dir}/submodules/geant4/${bb_geant4_version} \
    -G Ninja \
    -DCMAKE_INSTALL_PREFIX=${bb_bundle_dir}/${bb_build_geant4_tag} \
    -DCMAKE_INSTALL_DATADIR=${bb_bundle_dir}/share \
    -DGEANT4_INSTALL_DATADIR=${bb_bundle_dir}/geant4-data
  cmake \
    --build ${bb_build_dir}/${bb_build_geant4_tag} \
    --target install

  # - Build FSL variants against this config of Geant4
  # With PIC Geant4, we can enable the fullsimlightvialibrary option
  # and should build FSL libs in PIC mode
  fslvialib_option="OFF"
  fslvialib_pic_option=""
  if [[ "${v}" =~ ^.*pic$  ]] ; then
    fslvialib_option="ON"
    fslvialib_pic_option="-DCMAKE_POSITION_INDEPENDENT_CODE=ON"
  fi

  for f in ${bb_fsl_variants} ; do
    bb_build_fsl_tag=fullsimlight/${f}/${bb_build_geant4_tag}
    cmake \
      -C ${bb_source_dir}/scripts/build/fullsimlight-${f}.cmake \
      -B ${bb_build_dir}/${bb_build_fsl_tag} \
      -S ${bb_source_dir}/submodules/GeoModel \
      -G Ninja \
      -DGEOMODEL_BUILD_FULLSIMLIGHT_LIBRARY=${fslvialib_option} \
      ${fslvialib_pic_option} \
      -DCMAKE_INSTALL_PREFIX=${bb_bundle_dir}/${bb_build_fsl_tag} \
      -DCMAKE_PREFIX_PATH=${bb_bundle_dir}/${bb_build_geant4_tag}
    cmake \
      --build ${bb_build_dir}/${bb_build_fsl_tag} \
      --target install
  done
done

echo "[BuildBundle:INFO] Finished build phase..."

# - Install support scripts
echo "[BuildBundle:INFO] Installing scripts..."
cp ${bb_source_dir}/SetupForDevelopment.sh ${bb_bundle_dir}/.SetupForDevelopment.sh
cp ${bb_this_dir}/runFullSimLight.sh ${bb_bundle_dir}/runFullSimLight
${bb_this_dir}/MakeBundleSummary.sh ${bb_bundle_dir}/.buildmetadata

# Extract script for Geant4 data
cat << EOF > ${bb_bundle_dir}/.Geant4Data.sh
# Self-locate given how sourced
fslr_g4data_sourced=\$(dirname \${BASH_ARGV[0]})
FSL_BUNDLE_ROOT=\$(cd \$fslr_g4data_sourced > /dev/null ; pwd)
unset fslr_g4data_sourced
export FSL_BUNDLE_GEANT4_VERSION=${bb_geant4_version}
EOF

g4script=${bb_bundle_dir}/geant4/${bb_geant4_version}/multi/bin/geant4.sh
cat ${g4script} | \
  grep -E "^export G4.*DATA" | \
  sed s#${bb_bundle_dir}#\${FSL_BUNDLE_ROOT}#g >> ${bb_bundle_dir}/.Geant4Data.sh

# - Create tarball
echo "[BuildBundle:INFO] Creating tarball of bundle in ${bb_install_prefix}..."
tar -C ${bb_install_prefix} -zcf ${bb_install_prefix}/${bb_bundle_name}.tar.gz ${bb_bundle_name}

# - Cleanup
echo "[BuildBundle:INFO] Cleaning temporary files..."
rm -Rf ${bb_build_dir}

echo "[BuildBundle:INFO] Completed..."

