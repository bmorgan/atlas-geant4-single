#!/bin/bash
# Write a summary of the git status of the project and submodules at the point
# the bundle was built

# Input: File to write the summary to
mbs_file=$1

# Duplicate the .gitmodules file
mbs_root=$(cd `dirname $0` && git rev-parse --show-toplevel)
cp ${mbs_root}/.gitmodules $mbs_file

echo $mbs_root

# Extract submodule data
# Slightly awkward as we must run "git submodule" from the parent top level
data=$(cd $mbs_root && \
  git submodule foreach --quiet 'echo $name $sha1 `git describe $sha1 2>/dev/null || git describe --all $sha1`') 
printf '%s\n' "$data" | while read name sha1 desc ; do
  git config --file $mbs_file submodule.$name.commit $sha1
  git config --file $mbs_file submodule.$name.describe $desc
done

# Extract bundle builder data
git config --file $mbs_file bundle.commit `git --git-dir $mbs_root/.git rev-parse HEAD`
git config --file $mbs_file bundle.describe `git --git-dir $mbs_root/.git describe HEAD 2>/dev/null || git --git-dir $mbs_root/.git describe --all HEAD`
git config --file $mbs_file bundle.status `[[ -z $(git --git-dir $mbs_root/.git --work-tree $mbs_root status -s) ]] && echo "clean" || echo "modified"`
