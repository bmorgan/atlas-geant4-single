# Common cache file for ATLAS GeoModel/FullSimLight library builds
set(GEOMODEL_BUILD_FULLSIMLIGHT ON CACHE BOOL "")
set(GEOMODEL_BUILD_GEOMODELG4 ON CACHE BOOL "")
# Needed because LCG doesn't seem to supply this?
set(GEOMODEL_USE_BUILTIN_JSON ON CACHE BOOL "")
