# - SetupForDevelopment.sh
#
# Checks and sets up needed packages for testing Geant4
# single library builds with ATLAS/FullSimLight

# - Base root directory, release and platform
ag4_cvmfs_root="/cvmfs/sft.cern.ch/lcg/releases"
ag4_lcg_release="LCG_97apython3"
ag4_lcg_platform="x86_64-centos7-gcc8-opt"

# Unfortunately have to setup here using *known* version of package for
# given LCG release
declare -A ag4_lcg_pkgs=( \
  [CMake]="3.14.3" \
  [clhep]="2.4.1.3" \
  [zlib]="1.2.11" \
  [expat]="2.2.6" \
  [XercesC]="3.1.3" \
  [eigen]="3.3.7" \
  [sqlite]="3280000" \
  [ninja]="1.10.0" )

for K in "${!ag4_lcg_pkgs[@]}"
do
  echo "$K = ${ag4_lcg_pkgs[$K]}"
  source ${ag4_cvmfs_root}/${ag4_lcg_release}/${K}/${ag4_lcg_pkgs[$K]}/${ag4_lcg_platform}/${K}-env.sh
  pkghomevar="${K^^}__HOME"
  if [ -z "$CMAKE_PREFIX_PATH" ]
  then
    export CMAKE_PREFIX_PATH=${!pkghomevar}
  else
    export CMAKE_PREFIX_PATH=${CMAKE_PREFIX_PATH}:${!pkghomevar}
  fi
done

# ... and generators are separate again ...
#/cvmfs/sft.cern.ch/lcg/releases/LCG_97apython3/MCGenerators/pythia8/244/x86_64-centos7-gcc8-opt/
source ${ag4_cvmfs_root}/${ag4_lcg_release}/MCGenerators/pythia8/244/${ag4_lcg_platform}/pythia8env-genser.sh
export CMAKE_PREFIX_PATH=${CMAKE_PREFIX_PATH}:${PYTHIA8__HOME}
export PYTHIA8DATA="${PYTHIA8__HOME}/share/Pythia8/xmldoc"

echo "Done setting up..."
